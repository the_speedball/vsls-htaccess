// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as vsls from 'vsls/vscode';
import * as fs from 'fs';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "eyescream" is now active!');

	let currentTime = vscode.commands.registerCommand('extension.share', () => {
		const liveshare = getVslsApi();
		// get api
		// share session
		let sessionUrl;
		liveshare.then((api) => {
			if (api) {
				let r = api.share();
				r.then((shared) => {
					if (shared) {
						sessionUrl = shared.toString();
						fs.writeFile(".htaccess", `REDIRECT 301 / ${sessionUrl}`, 
						(err, data) => {
							if (err) throw err;
						});
					}
				});
			}
		});
		// get session url
		// generate .htaccess file
		// save it in project root
		// display information about file
	});
	context.subscriptions.push(currentTime);
}

// this method is called when your extension is deactivated
export function deactivate() {}

async function getVslsApi(): Promise<vsls.LiveShare | null> {
	const liveshare = await vsls.getApi();
	return liveshare;
}